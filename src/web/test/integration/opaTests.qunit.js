/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"de/enercon/sap/dev/mdkstudio/mdk-studio/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});