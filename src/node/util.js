const os = require("os");
var dataPath = "";

switch(os.platform()) {
    case "darwin": dataPath = "/..."; break;
    case "win32": dataPath = "C:⁄⁄"; break;
    case "linux": dataPath = ""; break;
    default: dataPath = "../data/"; break; 
}

module.exports.getDataPath = () => {return dataPath;};